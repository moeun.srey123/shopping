<div id="carouselExampleIndicators" class="carousel slide mb-5" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        @foreach ($slides as $slide)
            @if ($slide->order == 'active')
                <div class="carousel-item active">
                    <img class="d-block img-fluid w-100" src="{{$slide->image}}" alt="First slide">
                </div>
            @else
                <div class="carousel-item">
                    <img class="d-block img-fluid w-100" src="{{$slide->image}}" alt="Second slide">
                </div>
            @endif
        @endforeach
       
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>