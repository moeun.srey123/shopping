<h3 class="">Shop categories</h3>
<div class="list-group">
  @foreach ($categories_news as $category)
    <a href="{{url("news/$category->id/".str_slug($category->name))}}" class="list-group-item">{{$category->name}}</a>
  @endforeach
</div>