-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2020 at 03:50 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `srey`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, '2020-03-02 23:41:05', NULL, 'Clothes'),
(2, '2020-03-02 23:41:12', NULL, 'Jeans'),
(3, '2020-03-02 23:41:19', NULL, 'Bags'),
(4, '2020-03-02 23:41:33', NULL, 'Shoes'),
(5, '2020-03-02 23:41:40', NULL, 'Earring');

-- --------------------------------------------------------

--
-- Table structure for table `categories_news`
--

CREATE TABLE `categories_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories_news`
--

INSERT INTO `categories_news` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, '2020-03-03 02:52:07', NULL, 'Discount'),
(2, '2020-03-03 02:52:17', NULL, 'Promotion');

-- --------------------------------------------------------

--
-- Table structure for table `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2020-03-02 21:08:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2020-03-02 21:10:22', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/menus/add-save', 'Add New Data Home at menus', '', 1, '2020-03-02 21:18:32', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/menus/edit-save/1', 'Update data Home at menus', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>section</td><td>slide;news;product;</td><td>slide;news;product</td></tr></tbody></table>', 1, '2020-03-02 21:19:46', NULL),
(4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/menus/add-save', 'Add New Data Product at menus', '', 1, '2020-03-02 21:20:02', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/menus/add-save', 'Add New Data About at menus', '', 1, '2020-03-02 21:20:46', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/slides/add-save', 'Add New Data Fruits at slides', '', 1, '2020-03-02 23:11:31', NULL),
(7, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/slides/add-save', 'Add New Data Foods at slides', '', 1, '2020-03-02 23:11:44', NULL),
(8, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/slides/add-save', 'Add New Data animal at slides', '', 1, '2020-03-02 23:12:05', NULL),
(9, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/slides/delete/3', 'Delete data animal at slides', '', 1, '2020-03-02 23:25:20', NULL),
(10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/slides/delete/2', 'Delete data Foods at slides', '', 1, '2020-03-02 23:25:23', NULL),
(11, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/slides/delete/1', 'Delete data Fruits at slides', '', 1, '2020-03-02 23:25:44', NULL),
(12, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/slides/add-save', 'Add New Data Fruits at slides', '', 1, '2020-03-02 23:25:55', NULL),
(13, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/slides/add-save', 'Add New Data Foods at slides', '', 1, '2020-03-02 23:26:09', NULL),
(14, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/slides/add-save', 'Add New Data animal at slides', '', 1, '2020-03-02 23:26:19', NULL),
(15, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Clothes at categories', '', 1, '2020-03-02 23:41:06', NULL),
(16, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Jeans at categories', '', 1, '2020-03-02 23:41:12', NULL),
(17, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Bags at categories', '', 1, '2020-03-02 23:41:19', NULL),
(18, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Shoes at categories', '', 1, '2020-03-02 23:41:33', NULL),
(19, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/categories/add-save', 'Add New Data Earring at categories', '', 1, '2020-03-02 23:41:40', NULL),
(20, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/add-save', 'Add New Data Jeans at product', '', 1, '2020-03-03 00:07:23', NULL),
(21, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/edit-save/1', 'Update data Jeans at product', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p><br></p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"></p><p><br></p><p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p><br></p><p>         <img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">                <img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">               <img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"></p><p><br></p><p><span style=\"font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td></tr></tbody></table>', 1, '2020-03-03 01:09:30', NULL),
(22, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/edit-save/1', 'Update data Jeans at product', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>excerpt</td><td>Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit.</td><td>Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros.</td></tr></tbody></table>', 1, '2020-03-03 01:13:04', NULL),
(23, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/add-save', 'Add New Data SENTIMENT HIP at product', '', 1, '2020-03-03 01:23:40', NULL),
(24, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/add-save', 'Add New Data kitty at product', '', 1, '2020-03-03 01:27:04', NULL),
(25, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/add-save', 'Add New Data Pink for kids at product', '', 1, '2020-03-03 01:30:14', NULL),
(26, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/add-save', 'Add New Data Shoes for work at product', '', 1, '2020-03-03 01:34:48', NULL),
(27, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/add-save', 'Add New Data Midi skirt at product', '', 1, '2020-03-03 01:43:41', NULL),
(28, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2020-03-03 02:10:30', NULL),
(29, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2020-03-03 02:10:33', NULL),
(30, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/news/add-save', 'Add New Data  at News', '', 1, '2020-03-03 02:27:21', NULL),
(31, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/news/add-save', 'Add New Data  at News', '', 1, '2020-03-03 02:40:16', NULL),
(32, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/categories_news/add-save', 'Add New Data Discount at categories_news', '', 1, '2020-03-03 02:52:07', NULL),
(33, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/categories_news/add-save', 'Add New Data Promotion at categories_news', '', 1, '2020-03-03 02:52:17', NULL),
(34, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2020-03-03 18:34:18', NULL),
(35, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/section/add-save', 'Add New Data slides at section', '', 1, '2020-03-03 19:41:16', NULL),
(36, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/section/add-save', 'Add New Data Product at section', '', 1, '2020-03-03 19:41:23', NULL),
(37, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/section/add-save', 'Add New Data News at section', '', 1, '2020-03-03 19:41:28', NULL),
(38, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/menus/edit-save/1', 'Update data Home at menus', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>section_id</td><td>0</td><td>slides;Product;News</td></tr></tbody></table>', 1, '2020-03-03 19:49:40', NULL),
(39, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/menus/edit-save/2', 'Update data Product at menus', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>section_id</td><td>0</td><td>slides;Product;News</td></tr></tbody></table>', 1, '2020-03-03 20:17:14', NULL),
(40, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/menus/edit-save/1', 'Update data Home at menus', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>section_id</td><td>slides;Product;News</td><td>Product;News</td></tr></tbody></table>', 1, '2020-03-03 20:30:04', NULL),
(41, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/teams/add-save', 'Add New Data Test at teams', '', 1, '2020-03-03 20:47:42', NULL),
(42, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/teams/add-save', 'Add New Data Team at teams', '', 1, '2020-03-04 01:58:38', NULL),
(43, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/teams/add-save', 'Add New Data team 2 at teams', '', 1, '2020-03-04 01:58:56', NULL),
(44, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/teams/add-save', 'Add New Data team 3 at teams', '', 1, '2020-03-04 01:59:15', NULL),
(45, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/teams/add-save', 'Add New Data team 4 at teams', '', 1, '2020-03-04 01:59:38', NULL),
(46, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/teams/add-save', 'Add New Data team 5 at teams', '', 1, '2020-03-04 01:59:59', NULL),
(47, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/teams/delete/1', 'Delete data Test at teams', '', 1, '2020-03-04 02:00:15', NULL),
(48, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2020-03-04 20:05:46', NULL),
(49, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/section/add-save', 'Add New Data members at section', '', 1, '2020-03-04 20:06:45', NULL),
(50, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/menus/edit-save/1', 'Update data Home at menus', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>section_id</td><td>Product;News</td><td>Product;News;members</td></tr></tbody></table>', 1, '2020-03-04 20:10:20', NULL),
(51, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/news/edit-save/2', 'Update data  at News', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>slug</td><td></td><td>discount-30-up</td></tr><tr><td>categories_news_id</td><td></td><td>1</td></tr><tr><td>content</td><td><p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><br></p></td><td><p><span style=\"font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><span style=\"font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><br></p></td></tr></tbody></table>', 1, '2020-03-04 21:35:57', NULL),
(52, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/edit-save/1', 'Update data Jeans at product', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p><br></p><p>         <img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">                <img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">               <img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"></p><p><br></p><p><span style=\"font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p>&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td></tr></tbody></table>', 1, '2020-03-04 21:55:22', NULL),
(53, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/edit-save/1', 'Update data Jeans at product', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p>&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p>&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td></tr></tbody></table>', 1, '2020-03-04 21:57:08', NULL),
(54, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/edit-save/1', 'Update data Jeans at product', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p>&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p>&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">&nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td></tr></tbody></table>', 1, '2020-03-04 21:57:39', NULL),
(55, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/edit-save/1', 'Update data Jeans at product', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p>&nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">&nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p><img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">&nbsp; &nbsp; &nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">&nbsp; &nbsp; &nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td></tr></tbody></table>', 1, '2020-03-04 21:58:22', NULL),
(56, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36', 'http://localhost:8000/admin/products/edit-save/1', 'Update data Jeans at product', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p><img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">&nbsp; &nbsp; &nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">&nbsp; &nbsp; &nbsp;&nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td><td><p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p><img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">      <img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">      <img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p></td></tr></tbody></table>', 1, '2020-03-05 02:23:04', NULL),
(57, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'http://localhost:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2020-03-10 18:59:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'menus', 'Route', 'AdminMenusControllerGetIndex', NULL, 'fa fa-bars', 0, 1, 0, 1, 1, '2020-03-02 21:15:06', NULL),
(2, 'slides', 'Route', 'AdminSlidesControllerGetIndex', NULL, 'fa fa-file-image-o', 0, 1, 0, 1, 2, '2020-03-02 23:09:04', NULL),
(3, 'categories', 'Route', 'AdminCategoriesControllerGetIndex', NULL, 'fa fa-product-hunt', 0, 1, 0, 1, 3, '2020-03-02 23:37:23', NULL),
(4, 'product', 'Route', 'AdminProductsControllerGetIndex', NULL, 'fa fa-shopping-basket', 0, 1, 0, 1, 4, '2020-03-02 23:42:14', NULL),
(5, 'News', 'Route', 'AdminCategoriesNewsControllerGetIndex', NULL, 'fa fa-hacker-news', 0, 1, 0, 1, 5, '2020-03-03 02:03:32', NULL),
(6, 'News', 'Route', 'AdminNewsControllerGetIndex', NULL, 'fa fa-newspaper-o', 0, 1, 0, 1, 6, '2020-03-03 02:05:07', NULL),
(7, 'section', 'Route', 'AdminSectionControllerGetIndex', NULL, 'fa fa-table', 0, 1, 0, 1, 7, '2020-03-03 19:40:36', NULL),
(8, 'teams', 'Route', 'AdminTeamsControllerGetIndex', NULL, 'fa fa-users', 0, 1, 0, 1, 8, '2020-03-03 20:44:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2020-03-02 21:08:44', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2020-03-02 21:08:44', NULL, NULL),
(12, 'menus', 'fa fa-bars', 'menus', 'menus', 'AdminMenusController', 0, 0, '2020-03-02 21:15:06', NULL, NULL),
(13, 'slides', 'fa fa-file-image-o', 'slides', 'slides', 'AdminSlidesController', 0, 0, '2020-03-02 23:09:04', NULL, NULL),
(14, 'categories', 'fa fa-bar-chart', 'categories', 'categories', 'AdminCategoriesController', 0, 0, '2020-03-02 23:37:23', NULL, NULL),
(15, 'product', 'fa fa-shopping-basket', 'products', 'products', 'AdminProductsController', 0, 0, '2020-03-02 23:42:14', NULL, NULL),
(16, 'categories_news', 'fa fa-newspaper-o', 'categories_news', 'categories_news', 'AdminCategoriesNewsController', 0, 0, '2020-03-03 02:03:32', NULL, NULL),
(17, 'News', 'fa fa-newspaper-o', 'news', 'news', 'AdminNewsController', 0, 0, '2020-03-03 02:05:07', NULL, NULL),
(18, 'section', 'fa fa-table', 'section', 'section', 'AdminSectionController', 0, 0, '2020-03-03 19:40:36', NULL, NULL),
(19, 'teams', 'fa fa-users', 'teams', 'teams', 'AdminTeamsController', 0, 0, '2020-03-03 20:44:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-red', '2020-03-02 21:08:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 0, 1, 1, '2020-03-02 21:08:44', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2020-03-02 21:08:44', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2020-03-02 21:08:44', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2020-03-02 21:08:44', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2020-03-02 21:08:44', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2020-03-02 21:08:44', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2020-03-02 21:08:44', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2020-03-02 21:08:44', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2020-03-02 21:08:44', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2020-03-02 21:08:45', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2020-03-02 21:08:45', NULL),
(12, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(13, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(16, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(17, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(18, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(19, 1, 1, 1, 1, 1, 1, 19, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2020-03-02 21:08:45', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2020-03-02 21:08:45', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', NULL, 'upload_image', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2020-03-02 21:08:45', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2020-03-02 21:08:45', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'CRUDBooster', 'text', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2020-03-02 21:08:45', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', '', 'upload_image', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', '', 'upload_image', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2020-03-02 21:08:45', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', '', 'text', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', '', 'text', NULL, NULL, '2020-03-02 21:08:45', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', NULL, 'admin@crudbooster.com', '$2y$10$yYSdoiU79OgJiyivd74UI.55QzIxHwcv6IACsF5sdguXgJLO/J7gG', 1, '2020-03-02 21:08:44', NULL, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `created_at`, `updated_at`, `name`, `section_id`) VALUES
(1, '2020-03-02 21:18:31', '2020-03-04 20:10:20', 'Home', 'Product;News;members'),
(2, '2020-03-02 21:20:02', '2020-03-03 20:17:14', 'Product', 'slides;Product;News'),
(3, '2020-03-02 21:20:46', NULL, 'About', '0');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_08_07_145904_add_table_cms_apicustom', 1),
(2, '2016_08_07_150834_add_table_cms_dashboard', 1),
(3, '2016_08_07_151210_add_table_cms_logs', 1),
(4, '2016_08_07_151211_add_details_cms_logs', 1),
(5, '2016_08_07_152014_add_table_cms_privileges', 1),
(6, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(7, '2016_08_07_152320_add_table_cms_settings', 1),
(8, '2016_08_07_152421_add_table_cms_users', 1),
(9, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(10, '2016_08_07_154624_add_table_cms_moduls', 1),
(11, '2016_08_17_225409_add_status_cms_users', 1),
(12, '2016_08_20_125418_add_table_cms_notifications', 1),
(13, '2016_09_04_033706_add_table_cms_email_queues', 1),
(14, '2016_09_16_035347_add_group_setting', 1),
(15, '2016_09_16_045425_add_label_setting', 1),
(16, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(17, '2016_10_01_141740_add_method_type_apicustom', 1),
(18, '2016_10_01_141846_add_parameters_apicustom', 1),
(19, '2016_10_01_141934_add_responses_apicustom', 1),
(20, '2016_10_01_144826_add_table_apikey', 1),
(21, '2016_11_14_141657_create_cms_menus', 1),
(22, '2016_11_15_132350_create_cms_email_templates', 1),
(23, '2016_11_15_190410_create_cms_statistics', 1),
(24, '2016_11_17_102740_create_cms_statistic_components', 1),
(25, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(26, '2020_03_03_041217_create_menus', 2),
(27, '2020_03_03_045738_create_slides', 3),
(28, '2020_03_03_062811_create_products', 4),
(29, '2020_03_03_062850_create_categories', 4),
(30, '2020_03_03_090005_create_categories_news', 5),
(32, '2020_03_03_090108_create_news', 6),
(33, '2020_03_04_020208_create_section', 7),
(34, '2020_03_04_034108_create_teams', 8);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories_news_id` int(11) DEFAULT NULL,
  `cms_users_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `created_at`, `updated_at`, `name`, `slug`, `categories_news_id`, `cms_users_id`, `content`, `thumnail`, `excerpt`) VALUES
(1, '2020-03-03 02:27:21', NULL, 'Discount 50%', '', NULL, 1, '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><br></p>', 'uploads/1/2020-03/news_discount.jpg', 'Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.'),
(2, '2020-03-03 02:40:16', '2020-03-04 21:35:57', 'Discount 30% up', 'discount-30-up', 1, 1, '<p><span style=\"font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><span style=\"font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><br></p>', 'uploads/1/2020-03/news_discount.jpg', 'Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories_id` int(11) NOT NULL,
  `cms_users_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `created_at`, `updated_at`, `name`, `slug`, `categories_id`, `cms_users_id`, `content`, `thumnail`, `excerpt`) VALUES
(1, '2020-03-03 00:07:23', '2020-03-05 02:23:03', 'Jeans', '', 2, 1, '<p><img src=\"http://localhost:8000/uploads/1/2020-03/f88bea2fb0e65772d39aa10fa89b5b69.jpg\"></p><p><img src=\"http://localhost:8000/uploads/1/2020-03/576d746117cf51f88402aba956c8dafe.jpg\">      <img src=\"http://localhost:8000/uploads/1/2020-03/00492cc1302853e7372c46a3fcf307e3.jpg\">      <img src=\"http://localhost:8000/uploads/1/2020-03/8019e1249c0a8260a6fc88e252141ea3.jpg\"><br></p><p><br></p><p><span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros. Etiam lacus tortor, vestibulum auctor turpis ac, gravida viverra elit. Nam dui tellus, tincidunt at aliquam eu, hendrerit quis velit. Pellentesque eros diam, pellentesque sed neque ac, cursus feugiat turpis. Donec lobortis mattis odio, sit amet imperdiet libero fermentum sit amet. Phasellus id neque sit amet nunc semper gravida et non sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam a convallis velit, in porta est. Sed euismod purus a vehicula pellentesque. In eget quam a purus ultricies semper sed ac est. In gravida risus sed lectus dictum, id pharetra est imperdiet. Aenean varius ullamcorper sagittis. Curabitur pellentesque ante in nibh rutrum luctus.</span><br></p>', 'uploads/1/2020-03/jean_first.jpg', 'Sed venenatis elementum ante id luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ut nunc maximus, molestie nisl quis, sollicitudin eros.'),
(2, '2020-03-03 01:23:40', NULL, 'SENTIMENT HIP', '', 0, 1, '<p><img src=\"http://localhost:8000/uploads/1/2020-03/e0bcd062c3f8729c83e4d948fc8d4b1f.jpg\"></p><p><br></p><p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><br></p>', 'uploads/1/2020-03/bag_girl.jpg', 'Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Donec at quam elit. Donec a malesuada sem, ut venenatis velit.'),
(3, '2020-03-03 01:27:04', NULL, 'kitty', '', 4, 1, '<p><img src=\"http://localhost:8000/uploads/1/2020-03/2e7001d23b40c70af8b4efcb8f68c4fb.jpg\"></p><p><br></p><p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><br></p>', 'uploads/1/2020-03/shoes.jpg', 'Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Donec at quam elit. Donec a malesuada sem, ut venenatis velit.'),
(4, '2020-03-03 01:30:14', NULL, 'Pink for kids', '', 4, 1, '<p><img src=\"http://localhost:8000/uploads/1/2020-03/ef941b12b8fa31f3767199099562ad25.jpg\"></p><p><br></p><p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><br></p>', 'uploads/1/2020-03/shoes_kids_first.jpg', 'Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Donec at quam elit. Donec a malesuada sem, ut venenatis velit.'),
(5, '2020-03-03 01:34:48', NULL, 'Shoes for work', '', 4, 1, '<p><img src=\"http://localhost:8000/uploads/1/2020-03/2ce7b223c3d86d70e7b0d94d0a2b6477.jpg\"></p><p><br></p><p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><br></p>', 'uploads/1/2020-03/first_shoes_hieght.jpg', 'Donec at quam elit. Donec a malesuada sem, ut venenatis velit.Donec at quam elit. Donec a malesuada sem, ut venenatis velit.'),
(6, '2020-03-03 01:43:41', NULL, 'Midi skirt', '', 1, 1, '<p><img src=\"http://localhost:8000/uploads/1/2020-03/caf3fb678fa56c846a5dc6360afa7455.jpg\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/7f7511b11428587d6dae4b35f6d41cdb.jpg\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"http://localhost:8000/uploads/1/2020-03/376bef1b6f2c35beb94aa05a0f0c37d0.jpg\"></p><p><br></p><p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus. Fusce tortor ipsum, sollicitudin at nulla sed, sagittis vestibulum ante. Cras tempus eros et justo finibus ornare. Quisque facilisis arcu ut pretium cursus. Aliquam vitae mattis quam. Curabitur consectetur metus quis lorem auctor lacinia. Duis sit amet turpis erat. Ut felis enim, efficitur et enim a, sodales congue massa. Integer maximus bibendum felis a fermentum. Fusce malesuada eget nisi ac semper.</span><br></p>', 'uploads/1/2020-03/thum_midi.jpg', 'Donec at quam elit. Donec a malesuada sem, ut venenatis velit. Aenean convallis mauris at vehicula tempus.');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, '2020-03-03 19:41:16', NULL, 'slides'),
(2, '2020-03-03 19:41:23', NULL, 'Product'),
(3, '2020-03-03 19:41:28', NULL, 'News'),
(4, '2020-03-04 20:06:45', NULL, 'members');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `created_at`, `updated_at`, `name`, `image`, `order`) VALUES
(4, '2020-03-02 23:25:55', NULL, 'Fruits', 'uploads/1/2020-03/fruits_slide.jpg', 'active'),
(5, '2020-03-02 23:26:09', NULL, 'Foods', 'uploads/1/2020-03/foods_slide.jpg', 'sample'),
(6, '2020-03-02 23:26:19', NULL, 'animal', 'uploads/1/2020-03/dogs.jpg', 'sample');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `information` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `created_at`, `updated_at`, `name`, `profile`, `position`, `information`) VALUES
(2, '2020-03-04 01:58:38', NULL, 'Team', 'uploads/1/2020-03/team_01.jpg', 'Web Develop', '<p>testing</p>'),
(3, '2020-03-04 01:58:56', NULL, 'team 2', 'uploads/1/2020-03/team_02.jpg', 'Web Develop', '<p>testing</p>'),
(4, '2020-03-04 01:59:15', NULL, 'team 3', 'uploads/1/2020-03/team_03.jpg', 'Web Develop', '<p>team</p>'),
(5, '2020-03-04 01:59:38', NULL, 'team 4', 'uploads/1/2020-03/team_04.jpg', 'Web Develop', '<p>testing</p>'),
(6, '2020-03-04 01:59:58', NULL, 'team 5', 'uploads/1/2020-03/team_05.jpg', 'Web Develop', '<p>testing</p>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories_news`
--
ALTER TABLE `categories_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories_news`
--
ALTER TABLE `categories_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
