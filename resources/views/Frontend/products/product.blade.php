<div class="row">
    <div class="col-lg-12">
        <h2 class="text-center mb-5">Product Items</h2>
    </div>
    <div class="col-lg-3">
        {{-- categories --}}
        @include('frontend.products.category')
        {{-- end categories --}}
    </div>
    <!-- /.col-lg-3 -->

    <div class="col-lg-9">
      {{-- product --}}
      <div class="row">
          @foreach ($products as $product)
              <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card h-100">
                      <a href="#"><img class="card-img-top" src="{{$product->thumnail}}" alt=""></a>
                      <div class="card-body">
                      <h4 class="card-title">
                          <a href="{{url("article/".$product->id)}}">{{$product->name}}</a>
                      </h4>
                      <p class="card-text">{{str_limit(strip_tags($product->excerpt),100)}}</p>
                      </div>
                      <div class="card-footer">
                      <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                      </div>
                  </div>
              </div>
              
          @endforeach
      </div>    
      {{-- end product --}}

    </div>
</div>