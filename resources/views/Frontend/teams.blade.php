<div class="main mb-5">
    <div class="row teams">
      @foreach ($teams as $team)
          <div class="member">
              <img src="{{$team->profile}}" class="w-100 image">
              <div class="info">
                  <div class="text">
                    <h6>Name: {{$team->name}}</h6>
                    <h6>Position : {{$team->position}}</h6>
                    <h6>Information : </h6>
                    <p>{{$team->content}}</p>
                  </div>
              </div>
          </div>
      @endforeach
    </div>
  </div>
