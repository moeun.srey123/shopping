<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Shop Homepage - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('frontend/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('frontend/css/shop-homepage.css')}}" rel="stylesheet">

   <!-- Custom styles for this template -->
   <link href="{{asset('frontend/front/css/style.css')}}" rel="stylesheet">
   {{-- style slideslick --}}
   <link href="{{asset('frontend/slick/slick.css')}}" rel="stylesheet">
   <link href="{{asset('frontend/slick/slick-theme.css')}}" rel="stylesheet">

</head>

<body>
  
  <!-- Navigation -->
  @include('frontend.menu')
{{-- slideshow --}}
  {{-- @include('frontend.slide') --}}
{{-- endslideshow --}}
<?php
  $sections=(explode(";",$section->section_id));?> 
  @foreach ($sections as $section) 
    @if($section == 'slides')
      @include('frontend.slide')
    @endif
    <!-- Page Content -->
    <div class="container">
      @if($section == 'Product')
        @include('frontend.products.product')
      @endif
      @if($section == 'Product')
        @include('frontend.news.blog_news')
      @endif
      @if($section == 'members')
        {{-- blog teams member --}}
        @include('frontend.teams')
        {{-- end blog teams member --}}
      @endif
    </div>
  @endforeach


  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('frontend/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('frontend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
 {{-- js slideslick --}}
 <script src="{{asset('frontend/slick/slick.min.js')}}"></script>
 <script src="{{asset('frontend/slick/slick.js')}}"></script>
 {{-- <script src="{{asset('frontend/slick/jquery-1.11.0.min.js')}}"></script> --}}
 <script src="{{asset('frontend/slick/jquery-migrate-1.2.1.min.js')}}"></script>
</body>

</html>
