@extends('Frontend.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="text-center mb-3 mt-3">{{$product->name}} Detail</h2>
        </div>
        <div class="col-lg-3">
            {{-- categories --}}
            @include('frontend.products.category')
            {{-- end categories --}}
        </div>
        <!-- /.col-lg-3 -->
    
        <div class="col-lg-9">
          {{-- product --}}
          <div class="row">
              <div class="col-lg-12 col-md-12">
                  {!!$product->content!!}
              </div>
          </div>
          {{-- end product --}}
    
        </div>
    </div>
</div>
    
@endsection