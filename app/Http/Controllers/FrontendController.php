<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class FrontendController extends Controller
{
    public function getContent(){
        // menu
        $data['menus'] = DB::table('menus')->get();
        // slides
        $data['slides'] = DB::table('slides')->get();
        // categories
        $data['categories'] = DB::table('categories')->get();
        // product
        $data['products'] = DB::table('products')->orderby('products.id','desc') 
        ->take(9)
        ->get();
        // post news
        $data['news'] = DB::table('news')->orderby('news.id','desc')
            ->take(3)
            ->get();
        // categories news
        $data['categories_news'] = DB::table('categories_news')->get();
        // team members
        $data['teams'] = DB::table('teams')->get();

        return view('frontend.page', $data);
    }
    public function getMenu($slug){
           $data['section'] = DB::table('menus')->where('name', $slug)->first();
          // menu
          $data['menus'] = DB::table('menus')->get();
          // slides
          $data['slides'] = DB::table('slides')->get();
          // categories
          $data['categories'] = DB::table('categories')->get();
          // product
          $data['products'] = DB::table('products')->orderby('products.id','desc') 
          ->take(9)
          ->get();
          // post news
          $data['news'] = DB::table('news')->orderby('news.id','desc')
              ->take(3)
              ->get();
          // categories news
          $data['categories_news'] = DB::table('categories_news')->get();
           // team members
          $data['teams'] = DB::table('teams')->get();
  
          return view('frontend.page_menu', $data);
    }
    
    public function getCategory($id , $slug){
         // menu
         $data['menus'] = DB::table('menus')->get();
         // categories
         $data['categories'] = DB::table('categories')->get();
         $row =DB::table('categories')->where('id', $id)->first();
         $data['row'] = $row;
         $data['result'] = DB::table('products')->where('categories_id', $id)->get();

        return view('frontend.products.list_categories',$data);

    }

    public function getNews($id , $slug){
        // menu
        $data['menus'] = DB::table('menus')->get();
       // post news
       $data['news'] = DB::table('news')->where('categories_news_id', $id)->get();
        // categories news
        $data['categories_news'] = DB::table('categories_news')->get();
       return view('frontend.news.list_news',$data);

   }
   public function getProduct($id){
        // menu
        $data['menus'] = DB::table('menus')->get();
        $data['categories'] = DB::table('categories')->get();
        $data['product'] = DB::table('products')->where('id', $id)->first();
        return view('frontend.products.product_detail',$data);
   }

   public function getNewsDetail($id){
      // menu
      $data['menus'] = DB::table('menus')->get();
      // post news
      $data['news'] = DB::table('news')->where('id', $id)->first();
       // categories news
       $data['categories_news'] = DB::table('categories_news')->get();
      return view('frontend.news.news_detail',$data);
   }
}