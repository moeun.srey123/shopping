@extends('Frontend.master')
@section('content')
<div class="container">
    <div class="row mb-5">
        <div class="col-lg-12">
            <h2 class="text-center mb-5">List news</h2>
        </div>
        <div class="col-lg-3">
            {{-- categories --}}
            @include('frontend.news.news_categories')
            {{-- end categories --}}
        </div>
        <!-- /.col-lg-3 -->
    
        <div class="col-lg-9">
          {{-- product --}}
          <div class="row b-color">
              @foreach ($news as $ns)
                   <div class="col-lg-4 col-md-4 mb-4">
                    <a href="#"><img class="card-img-top" src="/{{$ns->thumnail}}" alt=""></a>
                   </div>
                    <div class="col-lg-8 col-md-d mb-4">
                      <div class="h-100">
                          <div class="card-body">
                            <h4 class="card-title">
                                <a href="{{url("article/news/".$ns->id)}}">{{$ns->name}}</a>
                            </h4>
                            <p class="card-text">{{str_limit(strip_tags($ns->excerpt),300)}}</p>
                          </div>                     
                      </div>
                    </div>
                    <hr/>
              @endforeach
          </div>    
          {{-- end product --}}
    
        </div>
    </div>
</div>
    
@endsection