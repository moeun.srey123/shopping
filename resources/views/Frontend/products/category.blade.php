        <h3 class="">Shop categories</h3>
        <div class="list-group">
          @foreach ($categories as $category)
            <a href="{{url("category/$category->id/".str_slug($category->name))}}" class="list-group-item">{{$category->name}}</a>
          @endforeach
        </div>